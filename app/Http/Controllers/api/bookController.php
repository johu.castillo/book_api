<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Transaction;

class BookController extends Controller
{
    /**
    * @api {post} /books/record Registrar un nuevo libro.
    * @apiName PostBookRecord
    * @apiGroup Books
    *
    * @apiParam {Number} id_author ID del autor del libro.
    * @apiParam {Number} name Nombre del libro.
    * @apiParam {Number} price Costo del libro.
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *       {
    *            "success": true,
    *            "message": "¡El libro se ha registrado exitosamente!",
    *            "content": {
    *                "id_author": 5,
    *                "name": "La divina comedia",
    *                "price": 20,
    *                "updated_at": "2021-03-22T22:19:41.000000Z",
    *                "created_at": "2021-03-22T22:19:41.000000Z",
    *                "id": 58
    *            }
    *        }
    */
    public function books_record(Request $request)
    {
        try {
            $book = Book::create($request->all());
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al registrar el libro.',
                'content' => $th->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'message' => '¡El libro se ha registrado exitosamente!',
            'content' => $book
        ], 200);
    }

    /**
    * @api {get} /books/get/:id? Obtener libros por ID de autor.
    * @apiName GetBooks
    * @apiGroup Books
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *    {
    *        "success": true,
    *        "content": [
    *            {
    *                "id": 8,
    *                "id_author": 3,
    *                "name": "La Montaña Mágica",
    *                "price": 35,
    *                "created_at": "2021-03-22T09:45:09.000000Z",
    *                "updated_at": "2021-03-22T09:45:09.000000Z",
    *                "author": {
    *                    "id": 3,
    *                    "name": "Zelma Graham",
    *                    "created_at": "2021-03-22T09:45:09.000000Z",
    *                    "updated_at": "2021-03-22T09:45:09.000000Z"
    *                },
    *                "discount_promotion": null,
    *                "help_promotion": null
    *            },
    *            {
    *                "id": 9,
    *                "id_author": 3,
    *                "name": "Confesiones de una Máscara",
    *                "price": 65,
    *                "created_at": "2021-03-22T09:45:09.000000Z",
    *                "updated_at": "2021-03-22T09:45:09.000000Z",
    *                "author": {
    *                    "id": 3,
    *                    "name": "Zelma Graham",
    *                    "created_at": "2021-03-22T09:45:09.000000Z",
    *                    "updated_at": "2021-03-22T09:45:09.000000Z"
    *                },
    *                "discount_promotion": null,
    *                "help_promotion": {
    *                    "id": 2,
    *                    "id_book": 9,
    *                    "start_date": "2021-03-24",
    *                    "total_quantity": 300,
    *                    "created_at": "2021-03-22T09:45:09.000000Z",
    *                    "updated_at": "2021-03-22T09:45:09.000000Z"
    *                }
    *            }
    *        ]
    *    }
    */
    public function books_get($id_author = null)
    {
        try {
            if ($id_author) {
                $books = Book::where('id_author', $id_author)->with(['author', 'discount_promotion', 'help_promotion'])->get();
            } else {
                $books = Book::with(['author', 'discount_promotion', 'help_promotion'])->get();
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al mostar la lista de libros.',
                'content' => $th->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'content' => $books
        ], 200);
    }

    /**
    * @api {delete} /books/delete/:id Eliminar un libro.
    * @apiName DelBook
    * @apiGroup Books
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *       {
    *            "success": true,
    *            "content": "¡El libro \"Confesiones de una Máscara\" se ha borrado exitosamente!"
    *        }
    */
    public function book_delete($id)
    {
        try {
            $book = Book::find($id);
            $name = $book->name;
            $book->delete();
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al borrar el libro.',
                'content' => $th->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'content' => '¡El libro "' . $name . '" se ha borrado exitosamente!'
        ], 200);
    }

    /**
    * @api {put} /books/update Actualizar un libro.
    * @apiName PutBook
    * @apiGroup Books
    *
    * @apiParam {Number} id_book ID del libro.
    * @apiParam {Number} name Nombre del libro.
    * @apiParam {Number} price Costo del libro.
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *       {
    *            "success": true,
    *            "message": "¡Los datos del libro se han actualizado exitosamente!",
    *            "content": {
    *                "id": 1,
    *                "id_author": 1,
    *                "name": "La llamada de Cthulhu",
    *                "price": "100.00",
    *                "created_at": "2021-03-22T09:45:09.000000Z",
    *                "updated_at": "2021-03-22T22:23:21.000000Z"
    *            }
    *        }
    */
    public function book_update(Request $request)
    {
        try {
            $book = Book::find($request->id_book);

            if (isset($request->name) && $book->name != $request->name) {
                $book->name = $request->name;
            }

            if (isset($request->price) && $book->price != $request->price) {
                $book->price = $request->price;
            }

            $book->save();
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al actualizar datos del libro.',
                'content' => $th->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'message' => '¡Los datos del libro se han actualizado exitosamente!',
            'content' => $book
        ], 200);
    }

    /**
    * @api {post} /books/buy Comprar un libro.
    * @apiName PostBuyBook
    * @apiGroup Books
    *
    * @apiParam {Number} id_book ID del libro.
    * @apiParam {Number} id_reader ID del lector que compra.
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *       {
    *            "success": true,
    *            "message": "¡El libro se ha comprado exitosamente!",
    *            "content": {
    *                "id_book": 6,
    *                "id_reader": 1,
    *                "amount": 35.25,
    *                "updated_at": "2021-03-22T22:37:13.000000Z",
    *                "created_at": "2021-03-22T22:37:13.000000Z",
    *                "id": 14
    *            }
    *        }
    */
    public function book_buy(Request $request)
    {
        try {
            $book = Book::with('discount_promotion')->findOrFail($request->id_book);
            $transaction = new Transaction;
            $transaction->id_book = $request->id_book;
            $transaction->id_reader = $request->id_reader;

            if (!is_null($book->discount_promotion)) {
                $transaction->amount = $book->price * ($book->discount_promotion->discount_percentage / 100);
            } else {
                $transaction->amount = $book->price;
            }

            $transaction->save();
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al comprar el libro.',
                'content' => $th->getMessage()
            ], 500);
        }

        return response()->json([
            'success' => true,
            'message' => '¡El libro se ha comprado exitosamente!',
            'content' => $transaction
        ], 200);
    }
}
