<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';
    protected $fillable = ['description', 'id_author'];

    /**
     * author function
     * Una notificación pertenece a un autor
     * @return void
     */
    public function author()
    {
        return $this->belongsTo(Author::class, 'id_author', 'id');
    }
}