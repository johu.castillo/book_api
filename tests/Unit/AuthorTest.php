<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Author;
use Illuminate\Database\Eloquent\Collection;

class AuthorTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_a_author_has_many_books()
    {
        $author = new Author;
        $this->assertInstanceOf(Collection::class, $author->book);
    }
}