<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Book;
use App\Models\Author;
use App\Models\DiscountPromotion;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Collection;


class BookTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_a_book_belongs_to_a_author()
    {
        $author = Author::create(['name' => Str::random(10)]);

        $book = Book::create([
            'id_author' => $author->id,
            'name' => Str::random(10),
            'price' => 0.0
        ]);

        $this->assertInstanceOf(Collection::class, $author->book);
    }
}