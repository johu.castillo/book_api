<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Transaction;
use App\Models\Donation;
use App\Models\HelpPromotion;
use App\Http\Controllers\api\bookController;
use Carbon\Carbon;

class promotionController extends Controller
{
    /**
    * @api {post} /promotions/donate Donar a una promoción de ayuda
    * @apiName PostDonation
    * @apiGroup Promotions
    *
    * @apiParam {Number} id_help_promotion ID de la promoción de ayuda.
    * @apiParam {Number} id_reader ID del lector.
    * @apiParam {Number} amount Cantidad a donar.
    *
    * @apiSuccessExample Success-Response:
    *     HTTP/1.1 200 OK
    *      {
    *          "success": true,
    *          "message": "¡La donación se ha realizado exitosamente!",
    *          "content": {
    *              "id_help_promotions": 1,
    *              "id_transaction": 12,
    *              "updated_at": "2021-03-22T22:05:49.000000Z",
    *              "created_at": "2021-03-22T22:05:49.000000Z",
    *              "id": 7
    *          }
    *      }
    */
    public function donate_to_promotion(Request $request)
    {   
        try {
            $helpPromotion = HelpPromotion::with('donations')->findOrFail($request->id_help_promotion);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'No se encontró la promoción de ayuda.',
                'content' => $th->getMessage()
            ], 500);
        }
    
        try {
            $date = Carbon::createFromFormat('Y-m-d', $helpPromotion->start_date);
            $actualDate = Carbon::now();
            if ($actualDate->greaterThan($date->copy()->addDays(60))) {
                return response()->json([
                    'success' => true,
                    'message' => 'Ya pasaron 60 días de haberse creado la promoción de ayuda.',
                    'content' => null
                ], 200);
            }
            
            if ($actualDate->lessThan($date->copy()->addDays(30))) {
                $totalQuantity = $helpPromotion->total_quantity * 0.6;
            }else {
                $totalQuantity = $helpPromotion->total_quantity;
            }

            $donatedAmount = 0;
            if ($request->amount <= 0) {
                return response()->json([
                    'success' => true,
                    'message' => 'La cantidad a donar debe ser mayor que cero.',
                    'content' => null
                ], 200);
            }

            if (!empty($helpPromotion->donations)) {
                foreach ($helpPromotion->donations as $key => $donation) {
                    $donatedAmount += $donation->transaction->amount;
                }
            }
            
            if ($totalQuantity <= $donatedAmount) {
                return response()->json([
                    'success' => true,
                    'message' => 'La promoción de ayuda no necesita más donaciones, ya alcanzó su tope. ¡Gracias!',
                    'content' => null
                ], 200);
            }

            if ($totalQuantity < ($donatedAmount + $request->amount)) {
                return response()->json([
                    'success' => true,
                    'message' => 'A esta promoción sólo le falta una donación de '. number_format($totalQuantity - $donatedAmount, 0, '.', '') .', tu donación supera el límite.',
                    'content' => null
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error en el proceso de donación. Por favor, revise los datos que se ingresaron.',
                'content' => $th->getMessage()
            ], 500);
        }
        
        try {
            $transaction = new Transaction;
            $transaction->id_reader = $request->id_reader;
            $transaction->amount = $request->amount;
            $transaction->save();

            $donation = new Donation;
            $donation->id_help_promotions = $request->id_help_promotion;
            $donation->id_transaction = $transaction->id;
            $donation->save();

            return response()->json([
                'success' => true,
                'message' => '¡La donación se ha realizado exitosamente!',
                'content' => $donation
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al donar en la promoción.',
                'content' => $th->getMessage()
            ], 500);
        }
    }
}
