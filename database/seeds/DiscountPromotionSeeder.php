<?php

use Illuminate\Database\Seeder;
use App\Models\DiscountPromotion;

class DiscountPromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $discount_promotion = DiscountPromotion::create([
            'id_book' => 6,
            'max_date' => '2021-04-23',
            'name' => 'Promoción día del libro',
            'discount_percentage' => 50
        ]);

        $discount_promotion = DiscountPromotion::create([
            'id_book' => 5,
            'max_date' => '2021-05-30',
            'name' => 'Promoción día de las Madres',
            'discount_percentage' => 25
        ]);

        $discount_promotion = DiscountPromotion::create([
            'id_book' => 2,
            'max_date' => '2021-06-30',
            'name' => 'Promoción día del Padre',
            'discount_percentage' => 20
        ]);
    }
}