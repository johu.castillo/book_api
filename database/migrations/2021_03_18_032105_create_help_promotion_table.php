<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHelpPromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_promotion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_book');
            $table->foreign('id_book')->references('id')->on('book')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->date('start_date');
            $table->float('total_quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_promotion');
    }
}