<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'book';
    protected $fillable = ['id_author', 'name', 'price'];

    /**
     * author function
     * Un libro pertenece a un autor
     * @return void
     */
    public function author()
    {
        return $this->belongsTo(Author::class, 'id_author', 'id');
    }

    /**
     * discount_promotion function
     * Un libro tiene una promoción de descuento a la vez
     * @return void
     */
    public function discount_promotion()
    {
        return $this->hasOne(DiscountPromotion::class, 'id_book', 'id');
    }

    /**
     * help_promotion function
     * Un libro se asocia a una promoción de ayuda a la vez
     * @return void
     */
    public function help_promotion()
    {
        return $this->belongsTo(HelpPromotion::class, 'id', 'id_book');
    }

    /**
     * transaction function
     * Un libro tiene muchas transacciones
     * @return void
     */
    public function transaction()
    {
        return $this->hasMany(Transaction::class, 'id_book', 'id')->with('reader');
    }
}