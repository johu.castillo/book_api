<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiscountPromotion extends Model
{
    protected $table = 'discount_promotion';
    protected $fillable = ['id_book', 'max_date', 'name', 'discount_percentage'];
    protected $primaryKey = 'id_book';

    /**
     * book function
     * Una promoción de descuento está en un libro a la vez
     * @return void
     */
    public function book()
    {
        return $this->hasOne(Book::class, 'id_book', 'id');
    }
}