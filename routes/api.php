<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'namespace' => 'api'], function () {
    Route::group(['prefix' => 'books'], function () {
        Route::post('record', 'BookController@books_record');
        Route::get('get/{id_autor?}', 'BookController@books_get');
        Route::post('buy', 'BookController@book_buy');
        Route::delete('delete/{id}', 'BookController@book_delete');
        Route::put('update', 'BookController@book_update');
    });

    Route::group(['prefix' => 'promotions'], function () {
        Route::post('donate', 'PromotionController@donate_to_promotion');
    });

    Route::group(['prefix' => 'transactions'], function () {
        Route::get('get/{id_autor}', 'TransactionController@books_report');
    });
});