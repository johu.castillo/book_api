<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Reader;

class ReaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $arrays = range(0, 6);

        foreach ($arrays as $value) {
            Reader::create([
                'name' => $faker->firstName(),
                'balance' => 100000 / rand(100, 1000)
            ]);
        }
    }
}