<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $table = 'donation';
    protected $fillable = ['id_help_promotions', 'id_transaction'];

    /**
     * transaction function
     * Una transacción pertenece una donación
     * @return void
     */
    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'id_transaction', 'id');
    }

    /**
     * help_promotions function
     * Una promoción de ayuda pertenece a una donación
     * @return void
     */
    public function help_promotions()
    {
        return $this->belongsTo(HelpPromotion::class, 'id_help_promotions', 'id');
    }
}