<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HelpPromotion extends Model
{
    protected $table = 'help_promotion';
    protected $fillable = ['id_book', 'start_date', 'total_quantity'];

    /**
     * book function
     * Un libro tiene una promoción de ayuda a la vez
     * @return void
     */
    public function book()
    {
        return $this->hasOne(Book::class, 'id_book', 'id');
    }

    /**
     * donations function
     * Una donación tiene muchas promociones de ayuda
     * @return void
     */
    public function donations()
    {
        return $this->hasMany(Donation::class, 'id_help_promotions', 'id')->with('transaction');
    }
}